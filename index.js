const express = require("express");
const app = express();
const { student } = require("./routes/studentRoute");
const { category } = require("./routes/categoryRoute");
const { roles } = require("./routes/roleRoute");

app.use(express.json()); //call for parsing application/json
app.use(express.urlencoded({ extended: true }));

student(app);
category(app);
roles(app);

const port = 8081;
app.listen(port, () => {
  console.log("http://localhost:" + port);
});
