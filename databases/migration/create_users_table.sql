CREATE TABLE users_table(
    id int(11) not null primary key auto_increment,
    role_id int(11) default null,
    user_name varchar(255) not null,
    password varchar(255) not null,
    isActive tinyint(1) default 1,
    created_at timestamp default CURRENT_TIMESTAMP,
    Foreign key(role_id) references role(id)
);