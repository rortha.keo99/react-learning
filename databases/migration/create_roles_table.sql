CREATE TABLE roles_table(
    id int (11) not null primary key auto_increment,
    title varchar(150) not null,
    description varchar(150) DEFAULT NULL,
    created_at timestamp default CURRENT_TIMESTAMP
);