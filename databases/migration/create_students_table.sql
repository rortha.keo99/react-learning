CREATE TABLE student_table(
    id int(11) not null primary key auto_increment,
    name varchar(150) not null,
    gender varchar(50) not null,
    dob date,
    address varchar(200) default null,
    created_at timestamp default current_timestamp()
);