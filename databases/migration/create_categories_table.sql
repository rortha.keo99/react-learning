CREATE TABLE categories_table(
    id int(11) not null primary key auto_increment,
    title varchar(200) not null,
    description varchar(200) default null,
    created_at timestamp default CURRENT_TIMESTAMP,
);