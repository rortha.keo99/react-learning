const { db } = require("../databases/connection");

const getList = async (req, res) => {
  const [data] = await db.query("SELECT * FROM roles_table");
  res.json({
    roleList: data,
  });
};

module.exports = {
  getList,
};
