const { db } = require("../databases/connection");

const getList = async (req, res) => {
  const [list] = await db.query("SELECT * FROM categories_table");
  const [count] = await db.query(
    "SELECT COUNT(id) AS TotalRecord FROM categories_table"
  );
  res.json({
    Count: count,
    category: list,
  });
};

const getOne = async (req, res) => {
  var paramId = {
    id: req.params.id,
  };
  const [data] = await db.query(
    "SELECT * FROM categories_table WHERE id = :id",
    paramId
  );
  res.json({
    ID_value: req.params,
    Data: data,
  });
};

const getCreate = async (req, res) => {
  const param = {
    title: req.body.title,
    description: req.body.description,
  };
  const sqlInsert =
    "INSERT INTO categories_table(title, description) VALUES(:title, :description)";
  const [data] = await db.query(sqlInsert, param);
  res.json({
    message: "Data saved..!",
    data: data,
  });
};

const getUpdate = async (req, res) => {
  const param = {
    id: req.body.id,
    title: req.body.title,
    description: req.body.description,
  };
  const sqlUpdate =
    "UPDATE categories_table SET title=:title, description=:description WHERE id = :id";
  const [data] = await db.query(sqlUpdate, param);
  res.json({
    message: "Data updated...!",
    data: data,
  });
};

const getRemove = async (req, res) => {
  const param = {
    id: req.body.id,
  };
  const sqlRemove = "DELETE FROM categories_table WHERE id = :id";
  const [data] = await db.query(sqlRemove, param);
  res.json({
    message: "Data deleted...!",
    data: data,
  });
};

module.exports = {
  getList,
  getOne,
  getCreate,
  getUpdate,
  getRemove,
};
