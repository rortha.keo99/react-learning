const { db } = require("../databases/connection");

const getList = async (req, res) => {
  const [list] = await db.query("SELECT * FROM student_table");
  const [count] = await db.query(
    "SELECT COUNT(id) AS TotalRecord FROM student_table"
  );
  res.json({
    message: "Successfull",
    totalRecord: count,
    studentList: list,
  });
};

const getOne = (req, res) => {
  res.json({});
};

const getCreate = (req, res) => {
  res.send("Create student");
};

const getUpdate = (req, res) => {
  res.send("Update student");
};

const getRemove = (req, res) => {
  res.send("Delete student");
};

module.exports = {
  getList,
  getCreate,
  getUpdate,
  getRemove,
  getOne,
};
