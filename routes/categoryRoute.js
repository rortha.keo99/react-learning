const {
  getList,
  getOne,
  getCreate,
  getUpdate,
  getRemove,
} = require("../controllers/categoryController");

const category = (app) => {
  app.get("/api/categories", getList);
  app.get("/api/categories/:id", getOne); // get by id(params)
  app.post("/api/categories", getCreate);
  app.put("/api/categories", getUpdate);
  app.delete("/api/categories", getRemove);
};

module.exports = {
  category,
};
