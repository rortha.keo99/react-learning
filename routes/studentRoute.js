const {
  getList,
  getCreate,
  getOne,
  getUpdate,
  getRemove,
} = require("../controllers/studentController");

const student = (app) => {
  app.get("/api/student", getList);
  app.get("/api/student/:id", getOne);
  app.post("/api/student", getCreate);
  app.put("/api/student", getUpdate);
  app.delete("/api/student", getRemove);
};

module.exports = {
  student,
};
