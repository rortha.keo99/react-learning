const { getList } = require("../controllers/roleController");

const roles = (app) => {
  app.get("/api/roles", getList);
};

module.exports = {
  roles,
};
